# Jhechtpack<small>: Jhecht's Personal Extensions</small>

I am Front-End Heavy Full Stack / DevOps Engineer. This is a very rough setup of what I have on basically any machine I do any development work on.

These are the extensions that I find myself using quite frequently, and for some reason I tend to have to re-install them all frequently, so I created this extension pack.

**Enjoy!**

## Vim

I use VIM frequently being a DevOps person, so from Version 0.0.7 onwards it is likely that the VIM extension for VSCode will be included. If you don't know or don't use VIM, then simply hit `ctrl+shift+x` and type `@installed vim` and uninstall the VIM extension.

That being said, I also got very used to the features of VSCode before finding this set of keybindings. That being said here is a rough list that I have changed (I use multiple computers, so it's tough to determine what a full list would be):

**Explanation**

1. I use the "add to selection" keybinding a lot &ndash; too much, probably.
2. There are a few chorded shortcuts that I use, so this fixes that for me.
3. I use the default "hide the sidebar" shortcut.

## Random Keybindings

`ctrl+alt+b` should be set to "Balance Outward"
`ctrl+alt+shift+b` should be set to "Balance Inward"


### VSCode

```
{
    "window.title": "${dirty}${activeEditorMedium}${separator}${rootName}",
    "editor.minimap.enabled": false,
    "breadcrumbs.enabled": true,
    "emmet.syntaxProfiles": {
        "html": {
            "tag_nl": true
        }
    },
    "vim.handleKeys": {
        "<C-d>": false,
        "<C-k>": false,
        "<C-b>": false
    },
    "vim.foldfix": true,
    "vim.normalModeKeyBindings": [
    {
      "before": [
        "leader",
        "n"
      ],
      "commands": [
        "lazy-new-file.lazyNewFile"
      ]
    },
    {
      "before": [
        "leader",
        "k"
      ],
      "commands": [
        "editor.action.showHover",
      ],
    },
    {
      "before": [
        "[",
        "d"
      ],
      "commands": [
        "editor.action.marker.prev"
      ]
    },
    {
      "before": [
        "]",
        "d"
      ],
      "commands": [
        "editor.action.marker.next"
      ]
    },
    {
      "before": [
        "leader",
        "r"
      ],
      "commands": [
        "editor.action.rename"
      ]
    },
    {
      "before": [
        "leader",
        "g",
        "t"
      ],
      "commands": [
        "workbench.view.scm"
      ]
    },
    {
      "before": [
        "leader",
        "t",
        "s"
      ],
      "commands": [
        "breadcrumbs.focusAndSelect"
      ]
    },
    {
      "before": [
        "leader",
        "w"
      ],
      "commands": [
        "workbench.action.files.save"
      ]
    }
  ]   
}
```
