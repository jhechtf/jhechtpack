# Change Log
All notable changes to the "jhechtpack" extension pack will be documented in this file.

## [1.1.0]

1. Adds in some more Vim configuration.
2. Cleans up README a little.

## [1.0.1]

1. Removed TSLint. TSLint is no longer maintained as a package, so it didn't make sense to keep it in the package.

## [0.0.2]

1. Updated README.md
2. Added in TSLint, Search Node Modules, and Laravel Blade to the pack

## [0.0.4]

1. Added in one of my own extensions to the pack (Jhecht's Git Emoji)

## [0.0.7]

1. Updated README.md file with general setting and information.
2. Removed the following extensions:
  * **New File:** I find myself more frustrated by this extension lately.
  * **Golang Support:** I don't use Golang as often as I previously did.
  * **Gitlens:** Didn't end up using a lot of the features of Gitlens to warrant keeping it around.
  * **PHP Debug / PHP Intellisense:** Not doing a lot of PHP work lately.